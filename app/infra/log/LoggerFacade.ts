// Facade Pattern
// Singleton Pattern

import {ConsoleLogger} from "./ConsoleLogger";
import {FileLogger} from "./FileLogger";
import {ErrorLogger} from "./ErrorLogger";
import {LogPriority, Logger} from "./Logger";

export class LoggerFacade {

    private static _chain: Logger = undefined;

    public static chain(): Logger {

        if(LoggerFacade._chain) return LoggerFacade._chain;

        let errorLogger = new ErrorLogger(LogPriority.Error);
        let fileLogger = new FileLogger(LogPriority.Debug);
        let consoleLogger = new ConsoleLogger(LogPriority.Info);

        errorLogger.next(fileLogger);
        fileLogger.next(consoleLogger);

        LoggerFacade._chain = errorLogger;

        return LoggerFacade._chain;
    }

}