// Chain of Responsability

import {Log} from "./Log";

export enum LogPriority {
    Info = 1,
    Debug = 2,
    Error = 3
}

export abstract class Logger {

    protected nextLogger: Logger = undefined;

    next(logger: Logger): void {
        this.nextLogger = logger;
    }

    message(level: LogPriority, log: Log): void {
        if(this.level <= level) this.write(log);
        if(this.nextLogger) this.nextLogger.message(level, log);
    }

    protected abstract write(log: Log): void;

    constructor(protected level: LogPriority){}

}