import {Logger} from "./Logger";
import {Log} from "./Log";

export class ConsoleLogger extends Logger {
    write(log: Log): void {
        console.log(log.message + " at " + log.time);
    }
}