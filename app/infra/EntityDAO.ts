import {DAO} from "./DAO";
import {Entity} from "../business/model/content/Entity";

export class EntityDAO<T extends Entity> implements DAO<Entity> {

    public insert(target: T): void {
        //...
    }

    public update(target: T): void {
        //...
    }
    
    public load(target: T): T {
        //...
        return undefined;
    }
    
    public remove(target: T): void {
        //...
    }

}