export interface DAO<T> {
    insert(target: T): void;
    update(target: T): void;
    load(target: T): T;
    remove(target: T): void;
}

