import {Entity} from "./model/content/Entity";
import {EntityProxy} from "./EntityProxy";
import {EntityFacade} from "./EntityFacade";
import {EntityDAO} from "../infra/EntityDAO";

export enum BuildType {
    Production = 1,
    Debug = 2
}

export class EntityBuilder<T extends Entity> {

    productionBuild(): EntityProxy<T> {       
        let dao = new EntityDAO<T>();
        let facade = new EntityFacade<T>(dao);
        return new EntityProxy<T>(facade);
    }

    debugBuild(): EntityProxy<T> {        
        let dao = new EntityDAO<T>();
        let facade = new EntityFacade<T>(dao);
        return new EntityProxy<T>(facade);        
    }

}