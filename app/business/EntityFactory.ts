import {Person} from "./model/content/Person";
import {Entity} from "./model/content/Entity";

export class EntityFactory {
    public static get<T>(): any {
        let arg: T;
        switch(arg.constructor.name) {
            case 'Person':
                return new Person;
            default:
                throw new Error("Unknow or Invalid Entity");
        }
    }
}