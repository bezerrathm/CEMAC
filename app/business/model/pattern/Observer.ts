// Observer Pattern

import {AbstractMethodException} from "../exceptions/AbstractMethodException";
import {Subject} from "./Subject";

export abstract class Observer {

    onCreate(): void {
        throw new AbstractMethodException;
    }

    onRead(): void {
        throw new AbstractMethodException;
    }

    onUpdate(): void {
        throw new AbstractMethodException;
    }

    onDelete(): void {
        throw new AbstractMethodException;
    }

    constructor(protected subject: Subject) {

    }

}