// State Pattern

import {Command} from "./Command";
import {Context} from "./Context";

export abstract class State<T> extends Command<T>  {

    protected abstract action(...args: any[]): any

    public execute(context: Context<T>, ...args: any[]): T {        
        context.state(this);
        return this.action(...args);
    }
}