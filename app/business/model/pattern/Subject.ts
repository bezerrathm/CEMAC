// Observer Pattern

import {Observer} from "./Observer";
import {State} from "./State";

export abstract class Subject {

    protected observers: Observer[] = [];

    protected notifyAll(state: State): void{
        switch(state) {
            case "onCreate":
                for(let o of this.observers) o.onCreate();
                break;
            case "onRead":
                for(let o of this.observers) o.onRead();
                break;
            case "onUpdate":
                for(let o of this.observers) o.onUpdate();
                break;
            case "onDelete":
                for(let o of this.observers) o.onDelete();
                break;                                                
            default:
                throw new Error("Unknow State: " + state);
        }
    }

    attach(observer: Observer): void {
        this.observers.push(observer);
    }
    
}