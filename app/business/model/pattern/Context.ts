// State Pattter 

import {Subject} from "./Subject";
import {State} from "./State";

export class Context<T> extends Subject {

    protected _state: State<T> = undefined;

    state(s?: State<T>): State<T> {
        if(s) {
            this._state = s;
            this.notifyAll(this._state);
        }
        else return this._state;
    }


}