import {Indexable} from "../feature/Indexable";
import {Entity} from "./Entity"

export class Person extends Entity implements Indexable {

    name: string;

    alias: string;

    birthday: Date;

    phone: string;

    email: string;

    street: string;

    locationId: string;

    region: string;

    country: string;
}