// Facade Pattern

import {State} from "./model/pattern/State";
import {Person} from "./model/content/Person";
import {DAO} from "../infra/DAO";
import {Context} from "./model/pattern/Context";
import {Entity} from "./model/content/Entity";

export class EntityFacade<T extends Entity> {

    public service(state: State<T>): T {
        return state.execute(this.context, this.dao);
    }

    constructor(private dao: DAO<T>, readonly context: Context<T> = new Context<T>()){}
}