import {DAO} from "../../infra/DAO";
import {State} from "../model/pattern/State";
import {Entity} from "../model/content/Entity";

export class DeleteEntity<T> extends State<T> {

    protected action(entityDAO: DAO<T>): T {
        entityDAO.remove(this.entity);
        return this.entity;
    }

    constructor(private entity: T) {
        super();
    }

}
