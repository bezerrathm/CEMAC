import {DAO} from "../../infra/DAO";
import {State} from "../model/pattern/State";
import {Entity} from "../model/content/Entity";

export class ReadEntity<T> extends State<T> {

    protected action(entityDAO: DAO<T>): T {
        return entityDAO.load(this.entity);        
    }

    constructor(private entity: T) {
        super();
    }

}