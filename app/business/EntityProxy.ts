// Proxy Pattern

import {EntityFacade} from "./EntityFacade";
import {EntityDAO} from "../infra/EntityDAO";
import {Entity} from "./model/content/Entity";
import {CRUD} from "./model/feature/CRUD";
import {EntityFactory} from "./EntityFactory";
import {ValidationFactory} from "../utils/validation/ValidationFactory";
import {CreateEntity} from "../business/control/CreateEntity";
import {ReadEntity} from "../business/control/ReadEntity";
import {UpdateEntity} from "../business/control/UpdateEntity";
import {DeleteEntity} from "../business/control/DeleteEntity";

export class EntityProxy<T extends Entity> implements CRUD {

    protected middle(entity: T): void {
        let entityValidation = ValidationFactory.get<T>();
        entityValidation.execute(entity);        
    }

    create(entity?: T): T {
        entity = (entity) ? entity : EntityFactory.get<T>();
        this.middle(entity);
        return this.facade.service(new CreateEntity<T>(entity));
    }

    read(entity?: T) {
        this.middle(entity);
        return this.facade.service(new ReadEntity<T>(entity));
    }

    update(entity: T) {
        this.middle(entity);
        return this.facade.service(new UpdateEntity<T>(entity));
    }

    delete(entity: T) {
        this.middle(entity);
        return this.facade.service(new DeleteEntity<T>(entity));        
    }

    constructor (protected facade: EntityFacade<T>) {

    }

}