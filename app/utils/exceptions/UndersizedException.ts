export class UndersizedException extends Error{
    constructor(paramName?: string, paramSize?: number) {
        super(((paramName) ? paramName : "") + " param has less than the minimal " + ((paramSize) ? paramSize : ("of " + paramSize)));
    }
}