export class OversizedException extends Error{
    constructor(paramName?: string, paramSize?: number) {
        super(((paramName) ? paramName : "") + " param has more than the limit " + ((paramSize) ? paramSize : ("of " + paramSize)));
    }
}