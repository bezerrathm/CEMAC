export class DatabaseUpdateException extends Error{
    constructor() {
        super("Could not update data into Database");
    }
}