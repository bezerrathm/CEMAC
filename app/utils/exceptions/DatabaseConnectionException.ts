export class DatabaseConnectionException extends Error{
    constructor() {
        super("Could not connect to the Database");
    }
}