export class DatabaseLoadException extends Error{
    constructor() {
        super("Could not load data from the Database");
    }
}