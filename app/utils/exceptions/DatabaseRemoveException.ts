export class DatabaseRemoveException extends Error{
    constructor() {
        super("Could not remove data from the Database");
    }
}