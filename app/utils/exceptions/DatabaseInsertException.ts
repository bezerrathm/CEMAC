export class DatabaseInsertException extends Error{
    constructor() {
        super("Could not insert data into the Database");
    }
}