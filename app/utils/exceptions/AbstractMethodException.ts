export class AbstractMethodException extends Error{
    constructor() {
        super("Failed trying to call an abstract or not implemented method");
    }
}
