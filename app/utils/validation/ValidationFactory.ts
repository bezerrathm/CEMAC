import {Person} from "../../business/model/content/Person";
import {PersonCheck} from "./PersonCheck";
import {Check} from "./Check";

export class ValidationFactory {
    public static get<T>(): Check<any> {
        let arg: T;
        switch(arg.constructor.name) {
            case 'Person':
                return new PersonCheck;
            default:
                throw new Error("Unknow or Invalid Entity");
        }
    }
}