import {Person} from "../../business/model/content/Person";
import {Check} from "./Check";

export class PersonCheck extends Check<Person> {

    public execute(person: Person) {
        //...
    }

    constructor(){
        super();
    }
}