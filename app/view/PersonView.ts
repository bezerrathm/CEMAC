import {EntityBuilder, BuildType} from "../business/EntityBuilder";
import {Person} from "../business/model/content/Person";
import {EntityProxy} from "../business/EntityProxy";

export class PersonView extends EntityBuilder<Person> {

    private proxy: EntityProxy<Person> = undefined;

    public behavior(type?: BuildType) {
        if(!type) return this.proxy;
        else {
            switch(type) {
                case BuildType.Production:
                    this.proxy = this.productionBuild();
                    break;
                case BuildType.Debug:
                    this.proxy = this.debugBuild();
                    break;
                default:
                    throw new Error("Unknow Production Type");
            }
        }
    }
    
}