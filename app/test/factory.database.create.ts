import {expect, describe, it} from "chai";
import * as DataBase from '../src/infra/factory/DataBase/';
import { User } from '../src/business/model/User/';
import { Receiver } from '../src/business/model/Pattern';

describe('Database Factory Teste', function() {
  describe('Padrão Factory', function() {
    it('Deve retornar uma instancia de Database', function(){

      let database = undefined;

      (new DataBase.Create([
          new (class extends Receiver {
              public action(...args: any[]): void {
                  if(args && args[0] && args[0]["connection"]) {
                      database = args[0];
                      console.log("Created Data Base Connection");
                  }                    
                  else {
                      console.log("Failed Establishing Data Base Connection");
                  }            
              }
          })            
      ], DataBase.MySQL).execute());      

      expect(database).to.be.an.instanceof(DataBase);
    });
  });
});