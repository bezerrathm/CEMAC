﻿# Documentação de Implementação

Padrões do projeto 2:
---------------------

Facade Pattern:

    app/business/EntityFacade.ts - fornece uma fachada para tratar serviços das entidades
    app/infra/log/LoggerFacade.ts - fornece uma fachada para construir a cadeia de Loggers

Command Pattern:
    Todos States


Singleton Pattern:
    app/infra/log/LoggerFacade.ts - instancia sempre o mesmo objeto

Template Pattern:
    Grande parte do código, utilizado principalmente para evitar o reuso e garantir a segurança de tipos

Factory Pattern:
    app/business/EntityFactory.ts - fornece a construção de entitdades sem a necessidade de conhecer como é feita

State Pattern:
    app/business/control/*.ts - fundido com o Command executa a ação e mantém atualiza o contexto


Padrões de Projeto 3:
---------------------

Chain of responsability:
    app/infra/*Logger.ts - define a hierarquia de transmissão dos Logs por tipo

Observer:
    Junto com o conceito de Subject, o Context (do State Pattern) extende a capacidade o Subject podendo se tornar um gerenciador/máquina de estados.

Proxy:
    app/business/EntityProxy.ts - gerencia a interface de uma Entitade para quem acessa ("outworld");

Builder:
    app/business/EntityBuilder.ts - cria o gerenciador de entidades utilizando muitos passos.
